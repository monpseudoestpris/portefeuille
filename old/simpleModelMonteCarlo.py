import sys
import math
import json
import argparse
import datetime
import os
import time
from optparse import OptionParser
import matplotlib
import matplotlib.pyplot as plt
import random
import numpy

INFINI=999999999

NB=5000

def interet_mensuel(taux):
    r=(1+taux/100)
    tmp=math.pow(r,1/12)
    out=(tmp-1)*100
    return(out)
def tirage_gauss_borne(vmin,vmax,mean,std,facteur):
    out=INFINI
    if not(vmin<mean<vmax):
        mean=(vmin+vmax)/2
    while(not(vmin<out<vmax)):
        out=random.gauss(mean,std)
    return(out*facteur)
def tirage_lineaire(vmin,vmax,tmp1,tmp2,tmp3):
    return(numpy.random.uniform(vmin,vmax))
def calcul_rendement_time(rdts,duree):
    rdtout=[]
    for i in range(duree):
        rdtout.append(0)
    
    for j in range(duree+1,len(rdts)):
        p=1
        for k in range(duree+1):
            p=p*(1+rdts[j-k]/100)
        #v=sum(rdts[j-duree:j])/duree
        vout=(math.pow(p,1/duree)-1)*100
        rdtout.append((vout))
    return(rdtout)

#transitions functions
def H_L(valeur,timer=0):
    probaout=0
    seuil1=tirage_gauss_borne(6000,7000,6500,2000,1)
    if valeur>seuil1:
         probaout=0.15+valeur/100000
    else:
        if valeur>5000 and timer>tirage_gauss_borne(12*8,12*12,12*10,6,1):
            probaout=0.05
    return(probaout)
def H_B(valeur,timer=0):
    probaout=0
    seuil1=tirage_gauss_borne(6000,7000,6500,2000,1)
    if valeur>seuil1:
         probaout=0.005+valeur/250000
    else:
        if valeur>5000 and timer>tirage_gauss_borne(12*8,12*12,12*10,6,1):
            probaout=0.015
    return(probaout)
def B_L(valeur,timer=0):
    probaout=0
    if valeur<6000 and timer>tirage_gauss_borne(12*0.25,12*2,12*1,6,1):
        probaout=0.05
    if valeur<4000 and timer>tirage_gauss_borne(12*0.25,12*2,12*1,6,1):
        probaout=0.1
    if valeur<2500 and timer>tirage_gauss_borne(12*0.25,12*2,12*1,6,1):
        probaout=0.2  
    return(probaout)



def L_B(valeur,timer=0):
    probaout=0
    if valeur>4000 and timer>tirage_gauss_borne(12*0.25,12,12*.5,8,1):
        probaout=0.05
    if valeur>6000 and timer>tirage_gauss_borne(12*0.25,12,12*.5,8,1):
        probaout=0.25   
    return(probaout)
def B_H(valeur,timer=0):
    probaout=0
    if valeur<6000 and timer>tirage_gauss_borne(12*0.25,12*2,12*1,6,1):
        probaout=0.05
    if valeur<4000 and timer>tirage_gauss_borne(12*0.25,12*2,12*1,6,1):
        probaout=0.1
    if valeur<2500 and timer>tirage_gauss_borne(12*0.25,12*2,12*1,6,1):
        probaout=0.2
    return(probaout)
def HF_H(valeur,timer=0):
    probaout=0
    if valeur>2000 and timer>tirage_gauss_borne(12*0.5,12*2,12*1,8,1):
        probaout=0.2
    return(probaout)
def L_H(valeur,timer=0):
    probaout=0
    if valeur>1000 and timer>tirage_gauss_borne(12*0.25,12,12*.5,8,1):
        probaout=0.05+valeur/200000
    if valeur<2500 and timer>tirage_gauss_borne(12*0.25,12,12*.5,8,1):
        probaout=0.2
            
    return(probaout)

        
def B_HF(valeur,timer=0):
    probaout=0
    if valeur<2500  and timer>tirage_gauss_borne(12*0.25,12*2,12*1,6,1):
        probaout=0.1
    return(probaout)
def H_HF(valeur,timer=0):
    probaout=0
    if valeur>2000:
        probaout=0.05
    return(probaout)
def L_HF(valeur,timer=0):
    probaout=0
    if valeur>1000 and timer>tirage_gauss_borne(12*0.25,12,12*.5,8,1):
        probaout=0.025
    return(probaout)

#rdt functions
def function_rdt_increase(old_rdt):
    m=random.random()*12
    return(tirage_gauss_borne(2,20,m,5,1))
def function_rdt_increase_high(old_rdt):
    m=random.random()*15
    return(tirage_gauss_borne(6,20,m,5,1.5))
def function_rdt_lateral(old_rdt):
    aleat=random.random()
    if 0<aleat<0.8:
        m=random.random()*8-4+old_rdt
    if 0.8<aleat<0.9:
        m=random.random()*8-1+old_rdt
    if 0.9<aleat<1.1:
        m=random.random()*8-9+old_rdt    
    return(tirage_gauss_borne(-3,3,m,3,1))
def function_rdt_decrease(old_rdt):
    m=random.random()*(-50)
    return(tirage_gauss_borne(-70,-10,m,30,1))

class fond():
    def __init__(self,name,duree,stateslist,fileParameters,valeur_init,mise_mensuelle):
        self.duree=duree
        self.name=name
        self.mise=mise_mensuelle
        self.table=[]
        self.states_list=stateslist
        self.valeur_init=valeur_init
        self.params=[x.split() for x in open(fileParameters,'r').readlines()  ]
        self.dico_params={}
        self.rdtslist=[]
        self.status="no"
        for l in self.params:
            self.dico_params[l[0]]={'function': l[1],'param1':(float)(l[2]),'param2':(float)(l[3]),'param3':(float)(l[4]),'param4':(float)(l[5])}
        f_tirage=''
        self.v=self.valeur_init
        self.table.append(self.v)
        self.rdt_annuel=0
    def calcul_evolution(self):
        for month in range(self.duree):
            f=  self.dico_params[self.states_list[month]]['function']
            if f=='gauss':
                f_tirage=tirage_gauss_borne
            if f=='linear':
                f_tirage=tirage_lineaire
            p1=self.dico_params[self.states_list[month]]['param1']
            p2=self.dico_params[self.states_list[month]]['param2']
            p3=self.dico_params[self.states_list[month]]['param3']
            p4=self.dico_params[self.states_list[month]]['param4']
            v1=f_tirage(p1,p2,p3,p4,1)
            v2=self.rdt_annuel

            if len(self.rdtslist)>=24:
                smallrdt=[x for x in self.rdtslist[-24:] if x<-10]
            else:
                smallrdt=[]
            if len(smallrdt)>12:
                meansmallrdt=sum(smallrdt)/len(smallrdt)
                self.rdt_annuel=-(meansmallrdt)*random.random()+tirage_lineaire(10,50,0,0,0)
            else:
                self.rdt_annuel=v1*0.9+0.1*v2
            
            self.rdtslist.append(self.rdt_annuel)
            self.rdt_mensuel=interet_mensuel(self.rdt_annuel)
            self.v=self.v*(1+self.rdt_mensuel/100)+self.mise
            if self.status=="source":
                if self.v>self.seuil:
                    toAdd=-(self.v-self.seuil)*self.delta
                    if self.v>-(toAdd):
                        self.v=self.v+toAdd
                        self.ecrete.append(-toAdd)
                    else:
                        self.ecrete.append(0)
                else:
                    self.ecrete.append(0)
            if self.status=="target":
                for ecre in self.flux.ecrete_list:
                    vtoadd=ecre[month]*self.delta
                    self.v=self.v+vtoadd
    
            self.table.append(self.v)
            
    def flux(self,somme_mensuelle,seuil,status,flux):
        pcttmp=somme_mensuelle.replace("pct","")
        pct=(float)(pcttmp)/100
        self.delta=pct
        self.seuil=seuil
        self.status=status #source ou target
        self.ecrete=[]
        self.flux=flux
        
        

############################
#class implementing an index with 4 states and the transitions
#in duree: months
class Index():
    def __init__(self,duree):
        self.valeur_init=tirage_gauss_borne(5000,7000,4500,2000,1)
        self.value=self.valeur_init
        self.states={}
        self.duree=duree
        self.table=[]
        self.possible_states=["H","HF","B","L"]
        self.statesList=[]
        #definition of states
        self.states["H"]=Etat("H")
        self.states["HF"]=Etat("HF")
        self.states["B"]=Etat("B")
        self.states["L"]=Etat("L")
        
        #transitions towards L
        self.states["H"].add_transition("L",H_L)
        self.states["HF"].add_transition("L",H_L)
        self.states["B"].add_transition("L",B_L)
        
        #transitions towards B
        self.states["H"].add_transition("B",H_B)
        self.states["HF"].add_transition("B",H_B)  
        self.states["L"].add_transition("B",L_B) 
        
        #transitions towards H
        self.states["B"].add_transition("H",B_H)
        self.states["HF"].add_transition("H",HF_H)
        self.states["L"].add_transition("H",L_H)
        #transitions towards HF
        self.states["B"].add_transition("HF",B_HF)
        self.states["H"].add_transition("HF",H_HF)
        self.states["L"].add_transition("HF",L_HF) 
        # addition of rdt 
        self.states["H"].rdt=function_rdt_increase
        self.states["HF"].rdt=function_rdt_increase_high
        self.states["L"].rdt=function_rdt_lateral
        self.states["B"].rdt=function_rdt_decrease
        
        #initial state is H
        self.state=self.states["H"]
    def get_display_state(self):
        return(self.state.name)
    def get_transitions(self):
        for e in self.state.transitions:
            print(self.state.name,"->",e,self.state.transitions[e](self.value))
    def generate_values(self):
        self.table.append(self.value)
        old_rdt=0
        for mois in range(duree):
            rdt_annual=self.state.rdt(old_rdt)
            rdt_monthly=interet_mensuel(rdt_annual)
            #if self.state.name=="L":
            #    print("---------------->")
            #print(mois,rdt_annual,rdt_monthly,self.state.name,self.state.timer,self.value)
            self.statesList.append(self.state.name)
            self.value=self.value*(1+rdt_monthly/100)
            self.table.append(self.value)
            delta=0
            self.state.timer+=1
            for e in self.state.transitions:
                p=self.state.transitions[e](self.value,self.state.timer)
                aleat=random.random()+delta
                #print("considering",e,"comparing proba",p,"with tirage",aleat)
                if aleat<p:
                    #print("changing state to",e)
                    if (self.state.name in ["H","HF"] and self.states[e].name in ["H","HF"]):
                        self.states[e].timer=self.state.timer
                        pass
                    else:
                        self.state.timer=0
                        self.states[e].timer=0
                    self.state=self.states[e]
                    delta=1.0
                    
                
            
    def plot(self):
        ax=plt.subplot()
        plt.plot(self.table)  
        ax.set_ylim(0,10000)
        plt.show()     
            
############################
#class implementing states
class Etat():
    def __init__(self,name):
        self.name=name
        self.transitions={} #dict with the transitions towards states with probabilities
        self.rdt={} #rdt associated to a state
        self.valeur=INFINI #valeur of index
        self.duration=0 #how much the state will last in months
        self.timer=0 #counter to measure since when the state lasts in months
    def add_transition(self,transition_name,proba):
        self.transitions[transition_name]=proba #proba may be a value or a function of valeur


class Flux_receiver():
    def __init__(self):
        self.ecrete_list=[]
    def add_source(self,ecrete):
        self.ecrete_list.append(ecrete)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--somme_depart", help="somme depart")
    parser.add_argument("--mise", help="mise mensuelle")
    parser.add_argument("--duree", help="duree in months")
    parser.add_argument("--fondeuro", help="proportion to fond euro")
    parser.add_argument("--fondsur", help="proportion to fond sur")
    parser.add_argument("--etf", help="proportion to etf")
    parser.add_argument("--fondfort", help="proportion to fond fort")
    parser.add_argument("--fondtresfort", help="proportion to fond tres fort")
    parser.add_argument("--etf2", help="proportion to fond effet levier")
    parser.add_argument("--immo", help="proportion to immo")    
    args = parser.parse_args()
    somme_depart=(float)(args.somme_depart)
    mise_mensuelle=(float)(args.mise)
    duree=(int)(args.duree)
    try:
        prop_fond_euro=(float)(args.fondeuro)
    except:
        prop_fond_euro=0
    try:
        prop_fond_sur=(float)(args.fondsur)
    except:
        prop_fond_sur=0
    try:    
        prop_etf=(float)(args.etf)
    except:
        prop_etf=0
    try:    
        prop_fond_fort=(float)(args.fondfort)
    except:
        prop_fond_fort=0
    try:    
        prop_fond_tres_fort=(float)(args.fondtresfort)
    except:
        prop_fond_tres_fort=0
    try:    
        prop_immo=(float)(args.immo)
    except:
        prop_immo=0
    try:    
        prop_etf2=(float)(args.etf2)
    except:
        prop_etf2=0
    
    if (prop_etf2+prop_fond_euro+prop_fond_sur+prop_etf+prop_fond_fort+prop_fond_tres_fort+prop_immo)>1.01 or (prop_etf2+prop_fond_euro+prop_fond_sur+prop_etf+prop_fond_fort+prop_fond_tres_fort+prop_immo)<0.99:
        print("problem with proportions")
        exit()
    
    #print("index defined with start value",cac40.valeur_init,cac40.get_display_state())
    #cac40.get_transitions()
    liste_simus_valeurs=[]
    liste_valeurs_fin=[]
    for simu in range(NB):
        cac40=Index(duree)
        cac40.generate_values()
        statesList=cac40.statesList
        
        mise_mensuelle_fond_euro=mise_mensuelle*prop_fond_euro
        mise_mensuelle_fond_sur=mise_mensuelle*prop_fond_sur
        mise_mensuelle_etf=mise_mensuelle*prop_etf
        mise_mensuelle_fond_fort=mise_mensuelle*prop_fond_fort
        mise_mensuelle_fond_tres_fort=mise_mensuelle*prop_fond_tres_fort
        mise_mensuelle_immo=mise_mensuelle*prop_immo
        mise_mensuelle_etf2=mise_mensuelle*prop_etf2
        

        
        fond_euro=fond("fond_euro",duree,statesList,"fond_euro.txt",somme_depart*prop_fond_euro,mise_mensuelle_fond_euro)
        fond_sur=fond("fond_sur",duree,statesList,"fond_sur.txt",somme_depart*prop_fond_sur,mise_mensuelle_fond_sur)
        etf=fond("etf",duree,statesList,"etf.txt",somme_depart*prop_etf,mise_mensuelle_etf)
        fond_fort=fond("fond_fort",duree,statesList,"fond_fort.txt",somme_depart*prop_fond_fort,mise_mensuelle_fond_fort)
        fond_tres_fort=fond("fond_tres_fort",duree,statesList,"fond_tresfort.txt",somme_depart*prop_fond_tres_fort,mise_mensuelle_fond_tres_fort)
        immo=fond("immo",duree,statesList,"immo_dividendes.txt",somme_depart*prop_immo,mise_mensuelle_immo)
        etf2=fond("etf2",duree,statesList,"etf2.txt",somme_depart*prop_etf2,mise_mensuelle_etf2)
        
        
        #funds where we remove money
        mes_flux=Flux_receiver()
        fond_tres_fort.flux("100pct",100000,"source",mes_flux)
        fond_fort.flux("100pct",100000,"source",mes_flux)
        etf2.flux("100pct",50000,"source",mes_flux)
        mes_flux.add_source(fond_tres_fort.ecrete)
        mes_flux.add_source(fond_fort.ecrete)
        mes_flux.add_source(etf2.ecrete)
        immo.flux("50pct",0,"target",mes_flux)
        fond_euro.flux("50pct",0,"target",mes_flux)
        
        etf.calcul_evolution()
        fond_fort.calcul_evolution()
        fond_tres_fort.calcul_evolution()
        etf2.calcul_evolution()
        #funds where we add money
        immo.calcul_evolution()
        fond_sur.calcul_evolution()
        fond_euro.calcul_evolution()

        
        table_fin=[]
        gain_m=[]
        rdts=[]
        for i in range(duree):
            table_fin.append(etf2.table[i]+immo.table[i]+fond_euro.table[i]+fond_sur.table[i]+etf.table[i]+fond_fort.table[i]+fond_tres_fort.table[i])
            
        for i,month in enumerate(table_fin):
            if i>0:
                gain_mensuel=(table_fin[i]-table_fin[i-1])-mise_mensuelle
            else:
                gain_mensuel=0
            if i>12:
                rdt=(table_fin[i]-table_fin[i-12]-mise_mensuelle*12)/(table_fin[i-12])*100
            else:
                rdt=0
            gain_m.append(gain_mensuel)
            rdts.append(rdt)
        liste_simus_valeurs.append(table_fin)
        liste_valeurs_fin.append(table_fin[-1])
    print("valeur minimale",min(liste_valeurs_fin),"rentes mensuelle 2pct 3pct 4pct",min(liste_valeurs_fin)*2/100/12,min(liste_valeurs_fin)*3/100/12,min(liste_valeurs_fin)*4/100/12)
    print("valeur maximale",max(liste_valeurs_fin),max(liste_valeurs_fin)*2/100/12,max(liste_valeurs_fin)*3/100/12,max(liste_valeurs_fin)*4/100/12)
    print("valeur moyenne",sum(liste_valeurs_fin)/NB,"rentes mensuelle 2pct 3pct 4pct",sum(liste_valeurs_fin)/NB*2/100/12,sum(liste_valeurs_fin)/NB*3/100/12,sum(liste_valeurs_fin)/NB*4/100/12)

    
    plt.figure(figsize=(14,8))
    plt.title('ARGENT')
    meanvu=False
    for t in liste_simus_valeurs:

        if t[-1]==min(liste_valeurs_fin) or t[-1]==max(liste_valeurs_fin) or abs(t[-1] -sum(liste_valeurs_fin)/NB )<5000:
            if t[-1]==min(liste_valeurs_fin):
                plt.plot(t,linewidth=10,color='red',label='min')
            if t[-1]==max(liste_valeurs_fin):
                plt.plot(t,linewidth=10,color='green',label='max')
            if abs(t[-1] -sum(liste_valeurs_fin)/NB )<5000 and meanvu==False:
                plt.plot(t,linewidth=10,color='orange',label='mean')
                meanvu=True
        else:
            plt.plot(t,linewidth=1,color='blue',alpha=0.05)
        
    plt.legend()
    plt.show()