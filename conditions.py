def always_false(valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base):
    return False


def condition_ecretage_world(
    valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base
):
    if valeur > seuil:
        return True
    else:
        return False


def condition_ecretage_world2(
    valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base
):
    if valeur > seuil:
        return True
    else:
        return False


def seuil_simple(valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base):
    if valeur > seuil:
        return True
    else:
        return False


def condition_ecretage_actions2(
    valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base
):
    last_dec_rdts = []
    WINDOW = 36
    try:
        deltaV = (dec_table[-1] - dec_table[-WINDOW]) / (dec_table[-WINDOW]) * 100
    except:
        deltaV = 0

    if len(declencheur_rdts) > WINDOW:
        last_dec_rdts = [x for x in declencheur_rdts[-WINDOW:]]
    else:
        return False
    avg_dec = sum(last_dec_rdts) / WINDOW
    if valeur > seuil:
        if len(declencheur_rdts) > 1:
            if avg_dec > 10:
                return True
            else:
                return False
        else:
            return False
    else:
        return False


def condition_ecretage_etf(
    valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base
):
    last_dec_rdts = []
    WINDOW = 48
    try:
        deltaV = (dec_table[-1] - dec_table[-WINDOW]) / (dec_table[-WINDOW]) * 100
    except:
        deltaV = 0
    if len(declencheur_rdts) > WINDOW:
        last_dec_rdts = [x for x in declencheur_rdts[-WINDOW:]]
    else:
        return False
    avg_dec = sum(last_dec_rdts) / WINDOW
    if valeur > seuil:
        if len(declencheur_rdts) > 1:
            if avg_dec > 5:
                return True
            else:
                return False
        else:
            return False
    else:
        return False


def condition_ecretage_etf2(
    valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base
):
    last_dec_rdts = []
    WINDOW = 36
    try:
        deltaV = (dec_table[-1] - dec_table[-WINDOW]) / (dec_table[-WINDOW]) * 100
    except:
        deltaV = 0
    if len(declencheur_rdts) > WINDOW:
        last_dec_rdts = [x for x in declencheur_rdts[-WINDOW:]]
    else:
        return False
    avg_dec = sum(last_dec_rdts) / WINDOW
    if valeur > seuil:
        if len(declencheur_rdts) > 1:
            if avg_dec > 15:
                return True
            else:
                return False
        else:
            return False
    else:
        return False


# strategie ou on ecrete le fond euro sur le cac passe sur un seuil
def condition_ecretage_fondrisque(
    valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base
):
    last_dec_rdts = []
    last_dec_rdts_verylast = []
    WINDOW = 12
    WINDOW2 = 3
    try:
        deltaV = (
            (declencheur_rdts[-1] - declencheur_rdts[-WINDOW])
            / (declencheur_rdts[-WINDOW])
            * 100
        )
    except:
        deltaV = 0
    if len(declencheur_rdts) > WINDOW:
        last_dec_rdts = [x for x in declencheur_rdts[-WINDOW:]]
        last_dec_rdts_verylast = [x for x in declencheur_rdts[-WINDOW2:]]
    else:
        return False
    avg_dec = sum(last_dec_rdts) / WINDOW
    avg_dec2 = sum(last_dec_rdts_verylast) / WINDOW2
    if (index_base.table[len(rdts) - 1] > 5000 and valeur > seuil) or (
        valeur > seuil * 1.5
    ):
        return "CAC40/0.99"

    return False


# strategie ou on ecrete le fond euro sur le cac passe sous un seuil
def condition_ecretage_fondeuro_cac(
    valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base
):
    last_dec_rdts = []
    last_dec_rdts_verylast = []
    WINDOW = 18
    WINDOW2 = 3
    try:
        deltaV = (
            (declencheur_rdts[-1] - declencheur_rdts[-WINDOW])
            / (declencheur_rdts[-WINDOW])
            * 100
        )
    except:
        deltaV = 0
    if len(declencheur_rdts) > WINDOW:
        last_dec_rdts = [x for x in declencheur_rdts[-WINDOW:]]
        last_dec_rdts_verylast = [x for x in declencheur_rdts[-WINDOW2:]]
    else:
        return False
    avg_dec = sum(last_dec_rdts) / WINDOW
    avg_dec2 = sum(last_dec_rdts_verylast) / WINDOW2
    if 3500 < index_base.table[len(rdts) - 1] < 4000 and valeur > seuil:
        return "CAC40/0.05"
    if 3000 < index_base.table[len(rdts) - 1] <= 3500 and valeur > seuil:
        return "CAC40/0.1"
    if index_base.table[len(rdts) - 1] < 3000 and valeur > seuil:
        return "CAC40/1.0"
    return False


# strategie ou on ecrete le fond euro en fonction des rendements d'un declencheur
def condition_ecretage_fondeuro(
    valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base
):
    last_dec_rdts = []
    last_dec_rdts_verylast = []
    WINDOW = 18
    WINDOW2 = 3
    try:
        deltaV = (
            (declencheur_rdts[-1] - declencheur_rdts[-WINDOW])
            / (declencheur_rdts[-WINDOW])
            * 100
        )
    except:
        deltaV = 0
    if len(declencheur_rdts) > WINDOW:
        last_dec_rdts = [x for x in declencheur_rdts[-WINDOW:]]
        last_dec_rdts_verylast = [x for x in declencheur_rdts[-WINDOW2:]]
    else:
        return False
    avg_dec = sum(last_dec_rdts) / WINDOW
    avg_dec2 = sum(last_dec_rdts_verylast) / WINDOW2

    if valeur > seuil:
        if len(declencheur_rdts) > 1:
            if avg_dec < -10 and avg_dec2 > 0:
                return True
            else:
                return False
        else:
            return False
    else:
        return False


def condition_ecretage_immo(
    valeur, seuil, rdts, declencheur_rdts, table, dec_table, index_base
):
    last_dec_rdts = []
    last_dec_rdts_verylast = []
    WINDOW = 18
    WINDOW2 = 3
    try:
        deltaV = (
            (declencheur_rdts[-1] - declencheur_rdts[-WINDOW])
            / (declencheur_rdts[-WINDOW])
            * 100
        )
    except:
        deltaV = 0
    if len(declencheur_rdts) > WINDOW:
        last_dec_rdts = [x for x in declencheur_rdts[-WINDOW:]]
        last_dec_rdts_verylast = [x for x in declencheur_rdts[-WINDOW2:]]
    else:
        return False
    avg_dec = sum(last_dec_rdts) / WINDOW
    avg_dec2 = sum(last_dec_rdts_verylast) / WINDOW2
    # print(last_dec_rdts,avg_dec)
    if valeur > seuil:
        if len(declencheur_rdts) > 1:
            if avg_dec < -10 and avg_dec2 > 0:
                return True
            else:
                return False
        else:
            return False
    else:
        return False
