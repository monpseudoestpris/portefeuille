import conditions

INFINI = 999999999999999999999

##############################
# ajout:somme que l'on rajoute chaque mois
# retrait: somme que l'on retire chaque mois
# condition_ecretage: fonction que retourne true ou false qui determine si on ecrete ou pas
# seuil_ecretage: somme à partir de laquelle lecretage s'applique
# pct_ecretage: pct du surplus que l'on ecrete
# declencheur: fond qui est utilise :comme declencheur pour lecretage
# liste_destinations: destination de l'ecretage. ce quon ecrete vas vers les fonds de cette liste
# mise: mise mensuelle
# duree_mise: nombre de mois pendant laquelle on fait la mise


class Flux:
    def __init__(
        self,
        ajout,
        retrait,
        condition_ecretage,
        seuil_ecretage,
        pct_ecretage,
        declencheur,
        liste_destinations,
        mise,
        duree_mise,
    ):
        self.ajout = ajout
        self.retrait = retrait
        self.condition_ecretage = condition_ecretage
        self.liste_destinations = liste_destinations  # liste (nom,pct)
        self.mise = mise
        self.duree_mise = duree_mise
        self.seuil_ecretage = seuil_ecretage
        self.declencheur = declencheur
        self.pct_ecretage = pct_ecretage


dico_strategies = {}


def agressif(
    fond_euro,
    mise_fond_euro,
    duree_mise_fond_euro,
    immo,
    mise_immo,
    duree_mise_immo,
    fond_sur,
    mise_fond_sur,
    duree_mise_fond_sur,
    etf,
    mise_etf,
    duree_mise_etf,
    etf2,
    mise_etf2,
    duree_mise_etf2,
    fond_fort,
    mise_fond_fort,
    duree_mise_fond_fort,
    world,
    mise_world,
    duree_mise_world,
):
    fond_euro_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_fondeuro,
        10000,
        1.0,
        world,
        [(world, 50), (fond_fort, 30), (etf, 10), (etf2, 10)],
        mise_fond_euro,
        duree_mise_fond_euro,
    )
    immo_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_immo,
        50000,
        1.0,
        world,
        [(world, 50), (fond_fort, 50)],
        mise_immo,
        duree_mise_immo,
    )
    fond_sur_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_fondeuro,
        10000,
        1.0,
        world,
        [(world, 50), (fond_fort, 50)],
        mise_fond_sur,
        duree_mise_fond_sur,
    )
    fond_fort_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_etf,
        100000,
        1.0,
        etf,
        [(fond_sur, 50), (fond_euro, 25), (immo, 25)],
        mise_fond_fort,
        duree_mise_fond_fort,
    )
    etf_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_etf,
        100000,
        1.0,
        etf,
        [(fond_sur, 50), (fond_euro, 25), (immo, 25)],
        mise_etf,
        duree_mise_etf,
    )
    etf2_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_etf2,
        20000,
        1.0,
        etf2,
        [(fond_sur, 25), (fond_euro, 25), (immo, 50)],
        mise_etf2,
        duree_mise_etf2,
    )
    world_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_world2,
        100000,
        1.0,
        world,
        [(fond_euro, 25), (etf, 25), (fond_sur, 25), (immo, 25)],
        mise_world,
        duree_mise_world,
    )
    return (
        fond_euro_flux,
        immo_flux,
        fond_sur_flux,
        fond_fort_flux,
        etf_flux,
        etf2_flux,
        world_flux,
    )


dico_strategies["AGRESSIF"] = agressif


def flux_test(
    fond_euro,
    mise_fond_euro,
    duree_mise_fond_euro,
    immo,
    mise_immo,
    duree_mise_immo,
    fond_sur,
    mise_fond_sur,
    duree_mise_fond_sur,
    etf,
    mise_etf,
    duree_mise_etf,
    etf2,
    mise_etf2,
    duree_mise_etf2,
    fond_fort,
    mise_fond_fort,
    duree_mise_fond_fort,
    world,
    mise_world,
    duree_mise_world,
):
    fond_euro_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_fondeuro,
        30000,
        0.9,
        world,
        [(world, 100)],
        mise_fond_euro,
        duree_mise_fond_euro,
    )
    immo_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_immo,
        duree_mise_immo,
    )
    fond_sur_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_fond_sur,
        duree_mise_fond_sur,
    )
    fond_fort_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_world2,
        100000,
        0.5,
        fond_fort,
        [(fond_sur, 50), (etf, 50)],
        mise_fond_fort,
        duree_mise_fond_fort,
    )
    etf_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_etf,
        duree_mise_etf,
    )
    etf2_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_world2,
        50000,
        0.9,
        etf2,
        [(immo, 25), (fond_sur, 25), (immo, 50)],
        mise_etf2,
        duree_mise_etf2,
    )
    world_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_world2,
        100000,
        0.5,
        world,
        [(immo, 100)],
        mise_world,
        duree_mise_world,
    )

    return (
        fond_euro_flux,
        immo_flux,
        fond_sur_flux,
        fond_fort_flux,
        etf_flux,
        etf2_flux,
        world_flux,
    )


dico_strategies["TEST"] = flux_test


def flux_simple(
    fond_euro,
    mise_fond_euro,
    duree_mise_fond_euro,
    immo,
    mise_immo,
    duree_mise_immo,
    fond_sur,
    mise_fond_sur,
    duree_mise_fond_sur,
    etf,
    mise_etf,
    duree_mise_etf,
    etf2,
    mise_etf2,
    duree_mise_etf2,
    fond_fort,
    mise_fond_fort,
    duree_mise_fond_fort,
    world,
    mise_world,
    duree_mise_world,
):
    fond_euro_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_fondeuro_cac,
        10000,
        0.95,
        world,
        [(fond_fort, 100)],
        mise_fond_euro,
        duree_mise_fond_euro,
    )
    immo_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_immo,
        duree_mise_immo,
    )
    fond_sur_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_fond_sur,
        duree_mise_fond_sur,
    )
    fond_fort_flux = Flux(
        0,
        0,
        conditions.seuil_simple,
        50000,
        0.05,
        fond_euro,
        [(fond_sur, 100)],
        mise_fond_fort,
        duree_mise_fond_fort,
    )
    etf_flux = Flux(
        0,
        0,
        conditions.seuil_simple,
        50000,
        0.05,
        fond_euro,
        [(immo, 50), (fond_sur, 50)],
        mise_etf,
        duree_mise_etf,
    )
    etf2_flux = Flux(
        0,
        0,
        conditions.seuil_simple,
        50000,
        0.05,
        world,
        [(immo, 50), (fond_sur, 50)],
        mise_world,
        duree_mise_world,
    )
    world_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_world2,
        100000,
        0.05,
        world,
        [(immo, 100)],
        mise_world,
        duree_mise_world,
    )

    return (
        fond_euro_flux,
        immo_flux,
        fond_sur_flux,
        fond_fort_flux,
        etf_flux,
        etf2_flux,
        world_flux,
    )


dico_strategies["SIMPLE"] = flux_simple


def flux_rien(
    fond_euro,
    mise_fond_euro,
    duree_mise_fond_euro,
    immo,
    mise_immo,
    duree_mise_immo,
    fond_sur,
    mise_fond_sur,
    duree_mise_fond_sur,
    etf,
    mise_etf,
    duree_mise_etf,
    etf2,
    mise_etf2,
    duree_mise_etf2,
    fond_fort,
    mise_fond_fort,
    duree_mise_fond_fort,
    world,
    mise_world,
    duree_mise_world,
):
    fond_euro_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0.95,
        world,
        [],
        mise_fond_euro,
        duree_mise_fond_euro,
    )
    immo_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_immo,
        duree_mise_immo,
    )
    fond_sur_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_fond_sur,
        duree_mise_fond_sur,
    )
    fond_fort_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0.5,
        fond_euro,
        [],
        mise_fond_fort,
        duree_mise_fond_fort,
    )
    etf_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_etf,
        duree_mise_etf,
    )
    etf2_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_etf2,
        duree_mise_etf2,
    )
    world_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        1.0,
        world,
        [(immo, 100)],
        mise_world,
        duree_mise_world,
    )

    return (
        fond_euro_flux,
        immo_flux,
        fond_sur_flux,
        fond_fort_flux,
        etf_flux,
        etf2_flux,
        world_flux,
    )


dico_strategies["RIEN"] = flux_rien


def flux_2020(
    fond_euro,
    mise_fond_euro,
    duree_mise_fond_euro,
    immo,
    mise_immo,
    duree_mise_immo,
    fond_sur,
    mise_fond_sur,
    duree_mise_fond_sur,
    etf,
    mise_etf,
    duree_mise_etf,
    etf2,
    mise_etf2,
    duree_mise_etf2,
    fond_fort,
    mise_fond_fort,
    duree_mise_fond_fort,
    world,
    mise_world,
    duree_mise_world,
):
    fond_euro_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_fondeuro_cac,
        100000,
        0.95,
        world,
        [(fond_fort, 50), (etf, 25), (etf2, 25)],
        mise_fond_euro,
        duree_mise_fond_euro,
    )
    immo_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_immo,
        duree_mise_immo,
    )
    fond_sur_flux = Flux(
        0,
        0,
        conditions.always_false,
        INFINI,
        0,
        fond_euro,
        [],
        mise_fond_sur,
        duree_mise_fond_sur,
    )
    fond_fort_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_fondrisque,
        200000,
        1.0,
        world,
        [(etf, 50), (fond_euro, 50)],
        mise_fond_fort,
        duree_mise_fond_fort,
    )
    etf_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_fondrisque,
        300000,
        0.5,
        world,
        [(etf, 50), (fond_euro, 50)],
        mise_etf,
        duree_mise_etf,
    )
    etf2_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_fondrisque,
        100000,
        1.0,
        world,
        [(etf, 50), (fond_euro, 50)],
        mise_etf2,
        duree_mise_etf2,
    )
    world_flux = Flux(
        0,
        0,
        conditions.condition_ecretage_fondrisque,
        100000,
        1.0,
        world,
        [(etf, 50), (fond_euro, 50)],
        mise_world,
        duree_mise_world,
    )

    return (
        fond_euro_flux,
        immo_flux,
        fond_sur_flux,
        fond_fort_flux,
        etf_flux,
        etf2_flux,
        world_flux,
    )


dico_strategies["2020"] = flux_2020
