import pf
import random
import matplotlib.pyplot as plt
import argparse

NB = 500


# EXEMPLE
# python3 launchSingle.py  --somme_depart 310000  --duree 240    --fondeuro 0.2/0/99999 --immo 0.1/0/9999 --etf 0.25/0/999 --fondsur 0.25/0/999 --fondfort 0.15/250/9999 --world 0.05/250/99999 --mode single --strategie AGRESSIF
# python3 launchSingle.py  --somme_depart 310000  --duree 240    --fondeuro 0.5/0/99999 --immo 0.2/0/9999 --etf 0.1/0/999 --fondfort 0.1/250/9999 --world 0.1/250/99999 --mode multi
# python3 launchSingle.py  --somme_depart 300000  --duree 240  --mode single  --fondeuro 0.3/50/99999  --etf 0.4/100/999 --fondfort 0.1/200/9999 --world 0.1/50/99999 --etf2 0.1/50/9999 --strategie RIEN
# python3 launchSingle.py  --somme_depart 1000  --duree 240  --mode single  --world  1.0/500/9999    --strategie RIEN --objectif 100000/200000
# python3 launchSingle.py  --somme_depart 0  --duree 240  --mode multi  --world  0.25/400/9999  --etf2 0.25/100/9999 --fondfort 0.25/100/9999 --etf 0.25/200/9999  --strategie RIEN
# python launchSingle.py  --somme_depart 0  --duree 480  --mode single   --etf 1.0/1000/156  --strategie RIEN

def main():
    parser = argparse.ArgumentParser()

    parser = argparse.ArgumentParser()
    parser.add_argument("--somme_depart", help="somme depart")
    parser.add_argument("--duree", help="duree in months")
    parser.add_argument("--fondeuro", help="proportion to fond euro")
    parser.add_argument("--fondsur", help="proportion to fond sur")
    parser.add_argument("--etf", help="proportion to etf")
    parser.add_argument("--fondfort", help="proportion to fond fort")
    parser.add_argument("--world", help="proportion etf world")
    parser.add_argument("--etf2", help="proportion to fond effet levier")
    parser.add_argument("--immo", help="proportion to immo")
    parser.add_argument("--mode", help="mode (single/multi)")
    parser.add_argument("--objectif", help="objectif final")
    parser.add_argument("--strategie", help="strategie")

    args = parser.parse_args()

    somme_depart = (float)(args.somme_depart)
    mode = (args.mode).split("/")[0]
    if mode == "multi" or mode == "batch":
        try:
            nb = (int)((args.mode).split("/")[1])
        except:
            nb = NB
    else:
        nb = 1
    duree = (int)(args.duree)
    try:
        objectif = args.objectif
    except:
        objectif = "0/99999999999999999"

    # launcher(somme_depart,mise,duree,fondeuro,fondsur,immo,etf,fondfort,world,etf2,mode)
    pf.launcher(
        somme_depart,
        duree,
        args.fondeuro,
        args.fondsur,
        args.immo,
        args.etf,
        args.fondfort,
        args.world,
        args.etf2,
        mode,
        args.strategie,
        nb,
        objectif,
    )


if __name__ == "__main__":
    # execute only if run as a script
    main()
