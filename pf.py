import sys
import math
import json
import argparse
import datetime
import os
import time
from optparse import OptionParser
import matplotlib
import matplotlib.pyplot as plt
import random
import numpy
import conditions
import flux

PLOT = True
INFINI = 999999999
NB = 500
INFLATION = 0 / 100
BAISSE_PERFORMANCE = (math.pow(1 + 0.1 / 100, 1 / 12) - 1.0) * 100
print("INFLATION", INFLATION, "%")
print("BAISSE_PERFORMANCE", BAISSE_PERFORMANCE)
BAISSE_MAX = 3
CAC40DEPARTMIN = 3000
CAC40DEPARTMAX = 7000

TRACE = True


def calcule_maxdrawdown(liste_valeurs):
    maxdd = 0
    l = len(liste_valeurs)
    for i, v in enumerate(liste_valeurs):
        for j in range(i, l):
            vj = liste_valeurs[j]
            try:
                gain = (vj - v) / v * 100
            except:
                gain = 0
            if gain < 0 and (-gain) > maxdd:
                maxdd = -gain

    return maxdd


def interet_mensuel(taux):
    r = 1 + taux / 100
    try:
        tmp = math.pow(r, 1 / 12)
    except:
        print("ERROR IN POW", r)
        exit()
    out = (tmp - 1) * 100
    return out


def tirage_gauss_borne(vmin, vmax, mean, std, facteur):

    out = INFINI
    if not (vmin < mean < vmax):
        mean = (vmin + vmax) / 2
    while out > vmax or out < vmin:
        out = random.gauss(mean, std)
    return out * facteur


def tirage_lineaire(vmin, vmax, tmp1, tmp2, tmp3):
    return numpy.random.uniform(vmin, vmax)


def calcul_rendement_time(rdts, duree):
    rdtout = []
    for i in range(duree):
        rdtout.append(0)

    for j in range(duree + 1, len(rdts)):
        p = 1
        for k in range(duree + 1):
            p = p * (1 + rdts[j - k] / 100)
        # v=sum(rdts[j-duree:j])/duree
        vout = (math.pow(p, 1 / duree) - 1) * 100
        rdtout.append((vout))
    return rdtout


def tire_valeur_autour(valeur, pct):
    delta = pct * valeur
    vmin = valeur - delta
    vmax = valeur + delta
    mean = valeur
    std = abs(valeur * pct)
    facteur = 1
    out = tirage_lineaire(vmin, vmax, mean, std, facteur)
    return out


def calcule_rendement_moyen(liste_rendements):
    o = 1
    cpt = 0
    for r in liste_rendements:
        v = 1 + r / 100
        o = o * v
        cpt += 1
    out = math.pow(o, 1 / cpt)
    out = out - 1
    out = out * 100.0
    return out


# transitions functions
def H_L(valeur, timer=0):
    probaout = 0
    seuil1 = tirage_gauss_borne(6000, 7000, 6500, 2000, 1)
    if valeur > seuil1:
        probaout = 0.15 + valeur / 100000
    else:
        if valeur > 5000 and timer > tirage_gauss_borne(12 * 8, 12 * 12, 12 * 10, 6, 1):
            probaout = 0.05
    return probaout


def H_B(valeur, timer=0):
    probaout = 0
    seuil1 = tirage_gauss_borne(6000, 7000, 6500, 2000, 1)
    if valeur > seuil1:
        probaout = 0.005 + valeur / 250000
    else:
        if valeur > 5000 and timer > tirage_gauss_borne(12 * 8, 12 * 12, 12 * 10, 6, 1):
            probaout = 0.015
    return probaout


def B_L(valeur, timer=0):
    probaout = 0
    if valeur < 6000 and timer > tirage_gauss_borne(12 * 0.25, 12 * 2, 12 * 1, 6, 1):
        probaout = 0.05
    if valeur < 3000 and timer > tirage_gauss_borne(12 * 0.25, 12 * 2, 12 * 1, 6, 1):
        probaout = 0.1
    if valeur < 2000 and timer > tirage_gauss_borne(12 * 0.25, 12 * 2, 12 * 1, 6, 1):
        probaout = 0.17
    return probaout


def L_B(valeur, timer=0):
    probaout = 0
    if valeur > 4000 and timer > tirage_gauss_borne(12 * 0.25, 12, 12 * 0.5, 8, 1):
        probaout = 0.05
    if valeur > 6000 and timer > tirage_gauss_borne(12 * 0.25, 12, 12 * 0.5, 8, 1):
        probaout = 0.25
    return probaout


def B_H(valeur, timer=0):
    probaout = 0
    if valeur < 6000 and timer > tirage_gauss_borne(12 * 0.25, 12 * 2, 12 * 1, 6, 1):
        probaout = 0.05
    if valeur < 3000 and timer > tirage_gauss_borne(12 * 0.25, 12 * 2, 12 * 1, 6, 1):
        probaout = 0.1
    if valeur < 2000 and timer > tirage_gauss_borne(12 * 0.25, 12 * 2, 12 * 1, 6, 1):
        probaout = 0.2
    return probaout


def HF_H(valeur, timer=0):
    probaout = 0
    if valeur > 2000 and timer > tirage_gauss_borne(12 * 0.5, 12 * 2, 12 * 1, 8, 1):
        probaout = 0.2
    return probaout


def L_H(valeur, timer=0):
    probaout = 0
    if valeur > 1000 and timer > tirage_gauss_borne(12 * 0.25, 12, 12 * 0.5, 8, 1):
        probaout = 0.05 + valeur / 200000
    if valeur < 2000 and timer > tirage_gauss_borne(12 * 0.25, 12, 12 * 0.5, 8, 1):
        probaout = 0.2

    return probaout


def B_HF(valeur, timer=0):
    probaout = 0
    if valeur < 2500 and timer > tirage_gauss_borne(12 * 0.25, 12 * 2, 12 * 1, 6, 1):
        probaout = 0.1
    return probaout


def H_HF(valeur, timer=0):
    probaout = 0
    if valeur > 2000:
        probaout = 0.05
    return probaout


def L_HF(valeur, timer=0):
    probaout = 0
    if valeur > 1000 and timer > tirage_gauss_borne(12 * 0.25, 12, 12 * 0.5, 8, 1):
        probaout = 0.025
    return probaout


# rdt functions
def function_rdt_increase(old_rdt):
    m = random.random() * 12
    return tirage_gauss_borne(2, 20, m, 5, 1)


def function_rdt_increase_high(old_rdt):
    m = random.random() * 15
    return tirage_gauss_borne(6, 20, m, 5, 1.5)


def function_rdt_lateral(old_rdt):
    aleat = random.random()
    if 0 < aleat < 0.8:
        m = random.random() * 8 - 4 + old_rdt
    if 0.8 < aleat < 0.9:
        m = random.random() * 8 - 1 + old_rdt
    if 0.9 < aleat < 1.1:
        m = random.random() * 8 - 9 + old_rdt
    return tirage_gauss_borne(-3, 3, m, 3, 1)


def function_rdt_decrease(old_rdt):
    m = random.random() * (-50)
    return tirage_gauss_borne(-70, -10, m, 30, 1)


class Portefeuille:
    def __init__(self, listOfFunds, flux, duree, indexBase):
        # listes ordonnees dans le meme ordre
        self.funds = listOfFunds
        self.flux = flux
        self.duree = duree
        self.all_values = []
        self.oldvs = {}
        self.indexe_base = indexBase

    def calcule_all(self):
        for month in range(self.duree):
            v = 0
            for fund in self.funds:
                v = v + fund.table[month]
            self.all_values.append(v)

    def calcule_evolutions_valeurs(self):
        for month in range(self.duree):
            self.ajustements_mensuels(month)
            self.evolution(month)

    def ajustements_mensuels(self, month):
        for ifund, fund in enumerate(self.funds):

            flux_fund = self.flux[ifund]

            delta = 0
            cond = flux_fund.condition_ecretage(
                fund.v,
                flux_fund.seuil_ecretage,
                fund.rdtslist,
                flux_fund.declencheur.rdtslist,
                fund.table,
                flux_fund.declencheur.table,
                self.indexe_base,
            )
            if isinstance(cond, str):
                if cond.find("CAC40") >= 0:
                    pct_cond = (float)(cond.split("/")[-1])
                    delta = (
                        (fund.v - flux_fund.seuil_ecretage)
                        * flux_fund.pct_ecretage
                        * pct_cond
                    )
                    if fund.v > delta and delta > 0:
                        oldv = fund.v
                        fund.v = fund.v - delta
                        newv = fund.v
                        if delta > 2000 and TRACE:
                            print(
                                month,
                                "transfering",
                                delta,
                                "from",
                                fund.name,
                                oldv,
                                "-->",
                                newv,
                            )

            if cond == True:
                delta = (fund.v - flux_fund.seuil_ecretage) * \
                    flux_fund.pct_ecretage
                if fund.v > delta and delta > 0:
                    oldv = fund.v
                    fund.v = fund.v - delta
                    newv = fund.v
                    if delta > 2000 and TRACE:
                        print(
                            month,
                            "transfering",
                            delta,
                            "from",
                            fund.name,
                            oldv,
                            "-->",
                            newv,
                        )

            if month <= flux_fund.duree_mise:
                fund.v = fund.v + flux_fund.ajout + flux_fund.mise
            else:
                fund.v = fund.v + flux_fund.ajout
            if fund.v > flux_fund.retrait:
                fund.v = fund.v - flux_fund.retrait
                somme_retraits = flux_fund.retrait + delta
            else:
                somme_retraits = delta

            if len(flux_fund.liste_destinations) > 0:
                for destination in flux_fund.liste_destinations:
                    dest = destination[0]
                    pct = destination[1]
                    oldvv = dest.v
                    dest.v = dest.v + pct / 100.0 * delta
                    newvv = dest.v
                    if delta > 2000 and TRACE:
                        print(
                            "---->",
                            month,
                            "transfering",
                            pct / 100.0 * delta,
                            "to",
                            dest.name,
                            "from",
                            fund.name,
                            "value change",
                            oldvv,
                            "-->",
                            newvv,
                        )

    def evolution(self, month):
        for ifund, fund in enumerate(self.funds):
            smallrdt = []
            smallrdt2 = []
            f = fund.dico_params[fund.states_list[month]]["function"]
            if f == "gauss":
                f_tirage = tirage_gauss_borne
            if f == "linear":
                f_tirage = tirage_lineaire
            p1 = fund.dico_params[fund.states_list[month]]["param1"]
            p2 = fund.dico_params[fund.states_list[month]]["param2"]
            p3 = fund.dico_params[fund.states_list[month]]["param3"]
            p4 = fund.dico_params[fund.states_list[month]]["param4"]
            v1 = 0
            if month % 12 == 0:
                v1 = f_tirage(p1, p2, p3, p4, 1)
                self.oldvs[fund] = v1
                # print(month,v1,fund.name)
            else:
                v1 = tire_valeur_autour(self.oldvs[fund], 0.2)
                # print("--->",month,v1,fund.name)

            v2 = fund.rdt_annuel

            if len(fund.rdtslist) >= 15:
                smallrdt = [x for x in fund.rdtslist[-15:]
                            if (x < -5 and x > -20)]
                smallrdt2 = [x for x in fund.rdtslist[-15:] if x < -20]
            else:
                smallrdt = []
                smallrdt2 = []
            if len(smallrdt) > 8 and v2 > 0:
                meansmallrdt = sum(smallrdt) / len(smallrdt)
                fund.rdt_annuel = -(meansmallrdt) * random.random() + tirage_lineaire(
                    10, 25, 0, 0, 0
                )
                self.oldvs[fund] = fund.rdt_annuel
            elif len(smallrdt2) > 8 and v2 > 0:
                meansmallrdt2 = sum(smallrdt2) / len(smallrdt2)
                fund.rdt_annuel = -(meansmallrdt2) * random.random() + tirage_lineaire(
                    10, 20, 0, 0, 0
                )
                self.oldvs[fund] = fund.rdt_annuel
            else:
                fund.rdt_annuel = v1 * 0.8 + 0.2 * v2
            baisse_perfo = min(
                (math.pow(BAISSE_PERFORMANCE / 100 + 1, month + 1)) - 1, BAISSE_MAX
            )
            # print("=======\n",fund.name,month,"baisse",baisse_perfo,(1-baisse_perfo))
            if fund.rdt_annuel > 0:
                # print("rdt depart",fund.name,fund.rdt_annuel)
                fund.rdt_annuel = (
                    fund.rdt_annuel * (1 - baisse_perfo)
                    - tire_valeur_autour(INFLATION, 0.33) * 100
                )
                # print("rdr fin",fund.name,"--->",fund.rdt_annuel)
                # print(month,"baisse",baisse_perfo,(1-baisse_perfo))
            else:
                fund.rdt_annuel = (
                    fund.rdt_annuel - tire_valeur_autour(INFLATION, 0.33) * 100
                )

            if fund.rdt_annuel < -95.0:
                fund.rdt_annuel = -95

            fund.rdtslist.append(fund.rdt_annuel)
            fund.rdt_mensuel = interet_mensuel(fund.rdt_annuel)
            fold = fund.v
            fund.v = fund.v * (1 + fund.rdt_mensuel / 100)
            deltav = fund.v - fold
            fund.list_deltas.append(deltav)
            fund.table.append(fund.v)
            # print(month,fund.name,fund.v)


class fond:
    def __init__(self, name, duree, stateslist, fileParameters, valeur_init):
        self.duree = duree
        self.name = name
        self.table = []
        self.states_list = stateslist
        self.valeur_init = valeur_init
        self.params = [x.split()
                       for x in open(fileParameters, "r").readlines()]
        self.dico_params = {}
        self.rdtslist = []
        self.list_deltas = [0]
        self.status = "no"
        for l in self.params:
            self.dico_params[l[0]] = {
                "function": l[1],
                "param1": (float)(l[2]),
                "param2": (float)(l[3]),
                "param3": (float)(l[4]),
                "param4": (float)(l[5]),
            }
        f_tirage = ""
        self.v = self.valeur_init
        self.table.append(self.v)
        self.rdt_annuel = 0


############################
# class implementing an index with 4 states and the transitions
# in duree: months
class Index:
    def __init__(self, duree):
        self.valeur_init = tirage_gauss_borne(
            CAC40DEPARTMIN, CAC40DEPARTMAX, 4000, 2000, 1
        )
        self.value = self.valeur_init
        self.states = {}
        self.duree = duree
        self.table = []
        self.possible_states = ["H", "HF", "B", "L"]
        self.statesList = []
        # definition of states
        self.states["H"] = Etat("H")
        self.states["HF"] = Etat("HF")
        self.states["B"] = Etat("B")
        self.states["L"] = Etat("L")

        # transitions towards L
        self.states["H"].add_transition("L", H_L)
        self.states["HF"].add_transition("L", H_L)
        self.states["B"].add_transition("L", B_L)

        # transitions towards B
        self.states["H"].add_transition("B", H_B)
        self.states["HF"].add_transition("B", H_B)
        self.states["L"].add_transition("B", L_B)

        # transitions towards H
        self.states["B"].add_transition("H", B_H)
        self.states["HF"].add_transition("H", HF_H)
        self.states["L"].add_transition("H", L_H)
        # transitions towards HF
        self.states["B"].add_transition("HF", B_HF)
        self.states["H"].add_transition("HF", H_HF)
        self.states["L"].add_transition("HF", L_HF)
        # addition of rdt
        self.states["H"].rdt = function_rdt_increase
        self.states["HF"].rdt = function_rdt_increase_high
        self.states["L"].rdt = function_rdt_lateral
        self.states["B"].rdt = function_rdt_decrease

        # initial state is H
        self.state = self.states["H"]

    def get_display_state(self):
        return self.state.name

    def get_transitions(self):
        for e in self.state.transitions:
            print(self.state.name, "->", e,
                  self.state.transitions[e](self.value))

    def generate_values(self):
        self.table.append(self.value)
        old_rdt = 0
        for mois in range(self.duree):
            rdt_annual = self.state.rdt(old_rdt)
            rdt_monthly = interet_mensuel(rdt_annual)
            # if self.state.name=="L":
            #    print("---------------->")
            # print(mois,rdt_annual,rdt_monthly,self.state.name,self.state.timer,self.value)
            self.statesList.append(self.state.name)
            self.value = self.value * (1 + rdt_monthly / 100)
            self.table.append(self.value)
            delta = 0
            self.state.timer += 1
            for e in self.state.transitions:
                p = self.state.transitions[e](self.value, self.state.timer)
                aleat = random.random() + delta
                # print("considering",e,"comparing proba",p,"with tirage",aleat)
                if aleat < p:
                    # print("changing state to",e)
                    if self.state.name in ["H", "HF"] and self.states[e].name in [
                        "H",
                        "HF",
                    ]:
                        self.states[e].timer = self.state.timer
                        pass
                    else:
                        self.state.timer = 0
                        self.states[e].timer = 0
                    self.state = self.states[e]
                    delta = 1.0

    def plot(self):
        ax = plt.subplot()
        plt.plot(self.table)
        ax.set_ylim(0, 10000)
        if PLOT:
            plt.show()


############################
# class implementing states
class Etat:
    def __init__(self, name):
        self.name = name
        self.transitions = (
            {}
        )  # dict with the transitions towards states with probabilities
        self.rdt = {}  # rdt associated to a state
        self.valeur = INFINI  # valeur of index
        self.duration = 0  # how much the state will last in months
        self.timer = 0  # counter to measure since when the state lasts in months

    def add_transition(self, transition_name, proba):
        self.transitions[
            transition_name
        ] = proba  # proba may be a value or a function of valeur


def launcher(
    somme_depart,
    duree,
    fondeuro,
    fondsur,
    immo,
    etf,
    fondfort,
    world,
    etf2,
    mode,
    strategie,
    nb,
    objectif="0/1000000000000",
):
    # parser = argparse.ArgumentParser()
    # parser.add_argument("--somme_depart", help="somme depart")
    # parser.add_argument("--mise", help="mise mensuelle")
    # parser.add_argument("--duree", help="duree in months")
    # parser.add_argument("--fondeuro", help="proportion to fond euro")
    # parser.add_argument("--fondsur", help="proportion to fond sur")
    # parser.add_argument("--etf", help="proportion to etf")
    # parser.add_argument("--fondfort", help="proportion to fond fort")
    # parser.add_argument("--world", help="proportion to fond tres fort")
    # parser.add_argument("--etf2", help="proportion to fond effet levier")
    # parser.add_argument("--immo", help="proportion to immo")
    # parser.add_argument("--mode", help="mode (single/multi)")

    # args = parser.parse_args()
    somme_depart = (float)(somme_depart)
    mode = mode
    duree = (int)(duree)
    mise_totale = 0
    try:
        strat = flux.dico_strategies[strategie]
    except:
        strat = flux.flux_onfait_rien
    try:
        objectifmin = (float)(objectif.split("/")[0])
        objectifmax = (float)(objectif.split("/")[1])
    except:
        objectifmin = 0
        objectifmax = 99999999999999999999999999999

    try:
        prop_fond_euro = (float)(fondeuro.split("/")[0])
        mise_fond_euro = (float)(fondeuro.split("/")[1])
        mise_totale += mise_fond_euro
        duree_mise_fond_euro = (float)(fondeuro.split("/")[2])
    except:
        prop_fond_euro = 0
        mise_fond_euro = 0
        mise_totale += mise_fond_euro
        duree_mise_fond_euro = INFINI
    try:
        prop_fond_sur = (float)(fondsur.split("/")[0])
        mise_fond_sur = (float)(fondsur.split("/")[1])
        mise_totale += mise_fond_sur
        duree_mise_fond_sur = (float)(fondsur.split("/")[2])
    except:
        prop_fond_sur = 0
        mise_fond_sur = 0
        mise_totale += mise_fond_sur
        duree_mise_fond_sur = INFINI
    try:
        prop_etf = (float)(etf.split("/")[0])
        mise_etf = (float)(etf.split("/")[1])
        mise_totale += mise_etf
        duree_mise_etf = (float)(etf.split("/")[2])
    except:
        prop_etf = 0
        mise_etf = 0
        mise_totale += mise_etf
        duree_mise_etf = INFINI
    try:
        prop_fond_fort = (float)(fondfort.split("/")[0])
        mise_fond_fort = (float)(fondfort.split("/")[1])
        mise_totale += mise_fond_fort

        duree_mise_fond_fort = (float)(fondfort.split("/")[2])
    except:
        prop_fond_fort = 0
        mise_fond_fort = 0
        mise_totale += mise_fond_fort
        duree_mise_fond_fort = INFINI
    try:
        prop_world = (float)(world.split("/")[0])
        mise_world = (float)(world.split("/")[1])
        mise_totale += mise_world
        duree_mise_world = (float)(world.split("/")[2])
    except:
        prop_world = 0
        mise_world = 0
        mise_totale += mise_world
        duree_mise_world = INFINI
    try:
        prop_immo = (float)(immo.split("/")[0])
        mise_immo = (float)(immo.split("/")[1])
        mise_totale += mise_immo
        duree_mise_immo = (float)(immo.split("/")[2])
    except:
        prop_immo = 0
        mise_immo = 0
        mise_totale += mise_immo
        duree_mise_immo = INFINI
    try:
        prop_etf2 = (float)(etf2.split("/")[0])
        mise_etf2 = (float)(etf2.split("/")[1])
        mise_totale += mise_etf2
        duree_mise_etf2 = (float)(etf2.split("/")[2])
    except:
        prop_etf2 = 0
        mise_etf2 = 0
        mise_totale += mise_etf2
        duree_mise_etf2 = INFINI
    if (
        prop_etf2
        + prop_fond_euro
        + prop_fond_sur
        + prop_etf
        + prop_fond_fort
        + prop_world
        + prop_immo
    ) > 1.01 or (
        prop_etf2
        + prop_fond_euro
        + prop_fond_sur
        + prop_etf
        + prop_fond_fort
        + prop_world
        + prop_immo
    ) < 0.99:
        if mode in ["single", "multi"]:
            print("problem with proportions")
            print(
                "missing",
                (
                    1
                    - (
                        prop_etf2
                        + prop_fond_euro
                        + prop_fond_sur
                        + prop_etf
                        + prop_fond_fort
                        + prop_world
                        + prop_immo
                    )
                ),
            )
        raise ("boom")

    if mode == "single":
        output = -9999
        outputmax = -9999
        cptrun = 0
        print(objectifmin, output, objectifmax)
        while output < objectifmin or output > objectifmax:
            cptrun += 1
            print("RUN", cptrun, objectifmin,
                  objectifmax, output, "max", outputmax)
            cac40 = Index(duree)
            # print("index defined with start value",cac40.valeur_init,cac40.get_display_state())
            # cac40.get_transitions()
            cac40.generate_values()
            statesList = cac40.statesList

            # mise_mensuelle_fond_euro=mise_mensuelle*prop_fond_euro
            # mise_mensuelle_fond_sur=mise_mensuelle*prop_fond_sur
            # mise_mensuelle_etf=mise_mensuelle*prop_etf
            # mise_mensuelle_fond_fort=mise_mensuelle*prop_fond_fort
            # mise_mensuelle_world=mise_mensuelle*prop_world
            # mise_mensuelle_immo=mise_mensuelle*prop_immo
            # mise_mensuelle_etf2=mise_mensuelle*prop_etf2

            fond_euro = fond(
                "fond_euro",
                duree,
                statesList,
                "fond_euro.txt",
                somme_depart * prop_fond_euro,
            )
            fond_sur = fond(
                "fond_sur",
                duree,
                statesList,
                "fond_sur.txt",
                somme_depart * prop_fond_sur,
            )
            etf = fond("etf", duree, statesList,
                       "etf.txt", somme_depart * prop_etf)
            fond_fort = fond(
                "fond_fort",
                duree,
                statesList,
                "fond_fort.txt",
                somme_depart * prop_fond_fort,
            )
            world = fond(
                "world", duree, statesList, "world.txt", somme_depart * prop_world
            )
            immo = fond(
                "immo",
                duree,
                statesList,
                "immo_dividendes.txt",
                somme_depart * prop_immo,
            )
            etf2 = fond("etf2", duree, statesList,
                        "etf2.txt", somme_depart * prop_etf2)

            # fond_euro_flux=Flux(0,0,conditions.always_false,INFINI,world,[],mise_fond_euro,duree_mise_fond_euro)
            (
                fond_euro_flux,
                immo_flux,
                fond_sur_flux,
                fond_fort_flux,
                etf_flux,
                etf2_flux,
                world_flux,
            ) = strat(
                fond_euro,
                mise_fond_euro,
                duree_mise_fond_euro,
                immo,
                mise_immo,
                duree_mise_immo,
                fond_sur,
                mise_fond_sur,
                duree_mise_fond_sur,
                etf,
                mise_etf,
                duree_mise_etf,
                etf2,
                mise_etf2,
                duree_mise_etf2,
                fond_fort,
                mise_fond_fort,
                duree_mise_fond_fort,
                world,
                mise_world,
                duree_mise_world,
            )

            PF = Portefeuille(
                [fond_euro, immo, fond_sur, fond_fort, etf, etf2, world],
                [
                    fond_euro_flux,
                    immo_flux,
                    fond_sur_flux,
                    fond_fort_flux,
                    etf_flux,
                    etf2_flux,
                    world_flux,
                ],
                duree,
                cac40,
            )

            table_fin = []
            gain_m = []

            PF.calcule_evolutions_valeurs()
            PF.calcule_all()
            for i in range(duree):
                s = 0
                for j in PF.funds:
                    s = j.table[i]
                table_fin.append(s)
            output = PF.all_values[-1]
            if outputmax < output:
                outputmax = output

        print("--->RUN", cptrun, objectifmin, objectifmax, output)
        plt.figure(figsize=(18, 6))
        plt.title("Funds")
        plt.grid()
        for fund in PF.funds:
            plt.plot(fund.table, label=fund.name, linewidth=3)
        plt.legend()

        plt.figure(figsize=(18, 6))
        plt.title("Rendement")
        for fund in PF.funds:
            if fund.table[-1] > 0:
                plt.plot(fund.rdtslist, label=fund.name, linewidth=3)
                rdtmoy = calcule_rendement_moyen(fund.rdtslist)
                print(
                    "Avg rdt",
                    fund.name,
                    rdtmoy,
                    min(fund.rdtslist),
                    "--->",
                    max(fund.rdtslist),
                )
        plt.legend()
        plt.grid()

        plt.figure(figsize=(18, 6))
        plt.title("Argent")
        plt.grid()
        plt.plot(PF.all_values, linewidth=3)
        plt.ylim(0, max(PF.all_values) + 50000)

        tdiff = []
        for i, x in enumerate(PF.all_values):
            if i > 1:
                d = x - PF.all_values[i - 1] - mise_totale
                tdiff.append(d)
            else:
                d = 0

        plt.figure(figsize=(18, 6))
        plt.title("Gain Mensuel")
        plt.grid()
        plt.plot(tdiff, linewidth=3)
        plt.ylim(min(tdiff) - 500, max(tdiff) + 500)

        plt.figure(figsize=(18, 6))
        plt.title("CAC40")
        plt.grid()
        plt.plot(cac40.table, linewidth=3)

        plt.figure(figsize=(18, 6))
        plt.title("Rente")
        t2 = [x * 2 / 100 / 12 for x in PF.all_values]
        t3 = [x * 3 / 100 / 12 for x in PF.all_values]
        t4 = [x * 4 / 100 / 12 for x in PF.all_values]
        t5 = [x * 5 / 100 / 12 for x in PF.all_values]
        t6 = [x * 6 / 100 / 12 for x in PF.all_values]
        plt.plot(t2, linewidth=3, label="2%")
        plt.plot(t3, linewidth=3, label="3%")
        plt.plot(t4, linewidth=3, label="4%")
        plt.plot(t5, linewidth=3, label="5%")
        plt.plot(t6, linewidth=3, label="6%")
        plt.ylim(0, max(t6) + 1000)
        plt.legend()
        plt.grid()
        plt.show()
    if mode == "multi" or mode == "batch":
        minvalue = INFINI
        maxvalue = -INFINI
        meanvalue = 0
        PFlist = []
        rdt_hash = {}
        mdd_hash = {}
        cacs = []
        for simu_nbr in range(nb):
            if simu_nbr % 100 == 0:
                print("simu", simu_nbr)
            cac40 = Index(duree)
            # print("index defined with start value",cac40.valeur_init,cac40.get_display_state())
            # cac40.get_transitions()
            cac40.generate_values()
            statesList = cac40.statesList

            # mise_mensuelle_fond_euro=mise_mensuelle*prop_fond_euro
            # mise_mensuelle_fond_sur=mise_mensuelle*prop_fond_sur
            # mise_mensuelle_etf=mise_mensuelle*prop_etf
            # mise_mensuelle_fond_fort=mise_mensuelle*prop_fond_fort
            # mise_mensuelle_world=mise_mensuelle*prop_world
            # mise_mensuelle_immo=mise_mensuelle*prop_immo
            # mise_mensuelle_etf2=mise_mensuelle*prop_etf2

            cacs.append(cac40.table)
            fond_euro = fond(
                "fond_euro",
                duree,
                statesList,
                "fond_euro.txt",
                somme_depart * prop_fond_euro,
            )
            fond_sur = fond(
                "fond_sur",
                duree,
                statesList,
                "fond_sur.txt",
                somme_depart * prop_fond_sur,
            )
            etf = fond("etf", duree, statesList,
                       "etf.txt", somme_depart * prop_etf)
            fond_fort = fond(
                "fond_fort",
                duree,
                statesList,
                "fond_fort.txt",
                somme_depart * prop_fond_fort,
            )
            world = fond(
                "world", duree, statesList, "world.txt", somme_depart * prop_world
            )
            immo = fond(
                "immo",
                duree,
                statesList,
                "immo_dividendes.txt",
                somme_depart * prop_immo,
            )
            etf2 = fond("etf2", duree, statesList,
                        "etf2.txt", somme_depart * prop_etf2)

            # fond_euro_flux=Flux(0,0,conditions.always_false,INFINI,world,[],mise_fond_euro,duree_mise_fond_euro)
            (
                fond_euro_flux,
                immo_flux,
                fond_sur_flux,
                fond_fort_flux,
                etf_flux,
                etf2_flux,
                world_flux,
            ) = strat(
                fond_euro,
                mise_fond_euro,
                duree_mise_fond_euro,
                immo,
                mise_immo,
                duree_mise_immo,
                fond_sur,
                mise_fond_sur,
                duree_mise_fond_sur,
                etf,
                mise_etf,
                duree_mise_etf,
                etf2,
                mise_etf2,
                duree_mise_etf2,
                fond_fort,
                mise_fond_fort,
                duree_mise_fond_fort,
                world,
                mise_world,
                duree_mise_world,
            )

            PF = Portefeuille(
                [fond_euro, immo, fond_sur, fond_fort, etf, etf2, world],
                [
                    fond_euro_flux,
                    immo_flux,
                    fond_sur_flux,
                    fond_fort_flux,
                    etf_flux,
                    etf2_flux,
                    world_flux,
                ],
                duree,
                cac40,
            )

            table_fin = []
            gain_m = []

            PF.calcule_evolutions_valeurs()
            PF.calcule_all()

            for fund in PF.funds:
                if fund.table[-1] > 0:
                    rdtmoy = calcule_rendement_moyen(fund.rdtslist)
                    if not (fund.name in rdt_hash):
                        rdt_hash[fund.name] = []
                    else:
                        rdt_hash[fund.name].append(rdtmoy)
                    mdd = calcule_maxdrawdown(fund.table)
                    if not (fund.name in mdd_hash):
                        mdd_hash[fund.name] = []
                    else:
                        mdd_hash[fund.name].append(mdd)

            for i in range(duree):
                s = 0
                for j in PF.funds:
                    s = j.table[i]
                table_fin.append(s)
            last = PF.all_values[-1]
            if last < minvalue:
                minvalue = last
            if last > maxvalue:
                maxvalue = last
            PFlist.append(PF.all_values)
        if mode == "multi":
            summ = 0
            for simu_nbr in range(nb):
                last = PFlist[simu_nbr][-1]
                summ += last
            mean_value = summ / nb
            print("average of end value of investment", mean_value)

            plt.figure(figsize=(12, 6))
            cpt_displayted = 0
            for simu_nbr in range(nb):
                aleat = random.random()
                seuil = 5 / NB
                if aleat < seuil and cpt_displayted < 10:
                    plt.plot(cacs[simu_nbr], alpha=0.7, color="g")
                    cpt_displayted = cpt_displayted + 1
                else:
                    plt.plot(cacs[simu_nbr], alpha=0.1, color="r")
            cpt = 0
            plt.grid()
            plt.title("CAC 40")
            plt.figure(figsize=(12, 6))
            for simu_nbr in range(nb):
                last = PFlist[simu_nbr][-1]
                if last == minvalue:
                    plt.plot(
                        PFlist[simu_nbr],
                        linewidth=3,
                        alpha=1.0,
                        color="red",
                        label="MIN",
                    )
                if last == maxvalue:
                    plt.plot(
                        PFlist[simu_nbr],
                        linewidth=3,
                        alpha=1.0,
                        color="green",
                        label="MAX",
                    )
                if mean_value * 0.99 < last < mean_value * 1.01 and cpt == 0:
                    plt.plot(
                        PFlist[simu_nbr],
                        linewidth=1,
                        alpha=1.0,
                        color="blue",
                        label="AVG",
                    )
                    cpt += 1
                elif mean_value * 0.99 < last < mean_value * 1.01 and cpt < 5:
                    plt.plot(PFlist[simu_nbr], linewidth=1,
                             alpha=1.0, color="blue")
                else:
                    plt.plot(PFlist[simu_nbr], linewidth=1,
                             alpha=0.05, color="blue")
            plt.grid()
            plt.legend()
            plt.title("ARGENT")
            for k in rdt_hash:
                print(
                    "average rdt of",
                    k,
                    sum(rdt_hash[k]) / len(rdt_hash[k]),
                    "between",
                    min(rdt_hash[k]),
                    "and",
                    max(rdt_hash[k]),
                    "with inflation",
                    INFLATION * 100.0,
                    "average max drawdown",
                    sum(mdd_hash[k]) / len(mdd_hash[k]),
                )
            if PLOT:
                plt.show()
        if mode == "batch":
            return PFlist
    # funds where we remove money
