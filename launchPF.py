import pf
import random
import matplotlib.pyplot as plt


# EXEMPLE
# python3 launchPF.py  --somme_depart 310000  --duree 240  --mode batch  --fondeuro 0.2/0/99999 --immo 0.3/0/9999 --etf 0.1/0/999 --fondfort 0.2/250/9999 --actions 0.2/250/99999
# pour voir les variations python3 launchSingle.py  --somme_depart 10000000  --duree 180    --fondeuro 0.1/1/99999 --immo 0.1/1/9999 --etf 0.1/1/999 --etf2 0.1/1/999 --fondsur 0.1/1/999 --fondfort 0.1/1/9999 --world 0.4/1/99999 --mode single --strategie RIEN


def arrondi(valeur):
    out = (int)(valeur * 100) / 100
    return out


def calcule_maxdrawdown(liste_valeurs):
    maxdd = 0
    l = len(liste_valeurs)
    for i, v in enumerate(liste_valeurs):
        for j in range(i, l):
            vj = liste_valeurs[j]
            try:
                gain = (vj - v) / v * 100
            except:
                gain = 0
            if gain < 0 and (-gain) > maxdd:
                maxdd = -gain

    return maxdd


def makeStr(valeur, mise):
    out = (str)(valeur) + "/" + (str)(mise) + "/999999"
    return out


def lance(
    fondeuro,
    immo,
    fondsur,
    fondfort,
    actions,
    etf,
    etf2,
    sommedepart,
    duree,
    misefondeuro,
    miseimmo,
    misefondsur,
    misefondfort,
    miseactions,
    miseetf,
    miseetf2,
):
    # print("TIRAGE",fondeuro,immo,fondsur,fondfort,actions,etf,etf2)
    somme = fondeuro + immo + fondsur + fondfort + actions + etf + etf2
    fondeurov = arrondi(fondeuro / somme)
    immov = arrondi(immo / somme)
    fondsurv = arrondi(fondsur / somme)
    fondfortv = arrondi(fondfort / somme)
    actionsv = arrondi(actions / somme)
    etfv = arrondi(etf / somme)
    etf2v = arrondi(etf2 / somme)
    somme = fondeurov + immov + fondsurv + fondfortv + actionsv + etfv + etf2v
    if somme > 1.01 or somme < 0.99:
        delta = 1.0 - somme
        fondeurov += delta
        fondeurov = arrondi(fondeurov)
    try:
        l = pf.launcher(
            sommedepart,
            duree,
            makeStr(fondeurov, misefondeuro),
            makeStr(fondsurv, misefondsur),
            makeStr(immov, miseimmo),
            makeStr(etfv, miseetf),
            makeStr(fondfortv, misefondfort),
            makeStr(actionsv, miseactions),
            makeStr(etf2v, miseetf2),
            "batch",
        )
        sum = 0
        cpt = 0
        min = INFINI
        max = -INFINI
        maxddmoy = 0
        smdd = 0
        for result in l:
            mdd = calcule_maxdrawdown(result)
            smdd += mdd
            # print("mdd",mdd)
            # plt.plot(result)
            # plt.show()
            valeur = result[-1]
            sum += valeur
            cpt += 1
            if valeur > max:
                max = (int)(valeur)
            if valeur < min:
                min = (int)(valeur)
        moyenne = (int)(sum / cpt)
        maxddmoy = smdd / cpt
        print(
            fondeurov,
            fondsurv,
            immov,
            etfv,
            fondfortv,
            actionsv,
            etf2v,
            min,
            moyenne,
            max,
            sommedepart,
            duree,
            maxddmoy,
            misefondeuro,
            miseimmo,
            misefondsur,
            misefondfort,
            miseactions,
            miseetf,
            miseetf2,
        )

    except:
        pass


misefondeuro = 0
miseimmo = 0
misefondsur = 0
misefondfort = 0
miseactions = 100
miseetf = 100
miseetf2 = 100
INFINI = 9999999999
print(
    "fondeuro",
    "fondsur",
    "immo",
    "etf",
    "fondfort",
    "actions",
    "etf2",
    "min",
    "moyenne",
    "max",
    "sommedepart",
    "mise",
    "duree",
    "maxdrawdown",
    "misefondeuro",
    "miseimmo",
    "misefondsur",
    "misefondfort",
    "miseactions",
    "miseetf",
    "miseetf2",
)
for etf2 in range(0, 30, 10):
    for fondeuro in range(1, 100, 20):
        for immo in range(0, 100, 20):
            for fondsur in range(0, 100, 20):
                for fondfort in range(0, 40, 10):
                    for actions in range(0, 30, 10):
                        for etf in range(0, 100, 20):

                            # print("TIRAGE",fondeuro,immo,fondsur,fondfort,actions,etf,etf2)
                            misefondeuro = 0
                            miseimmo = 0
                            misefondsur = 0
                            misefondfort = 50
                            miseactions = 100
                            miseetf = 0
                            miseetf2 = 0
                            lance(
                                fondeuro,
                                immo,
                                fondsur,
                                fondfort,
                                actions,
                                etf,
                                etf2,
                                345000,
                                20 * 12,
                                misefondeuro,
                                miseimmo,
                                misefondsur,
                                misefondfort,
                                miseactions,
                                miseetf,
                                miseetf2,
                            )
                            misefondeuro = 0
                            miseimmo = 0
                            misefondsur = 0
                            misefondfort = 50
                            miseactions = 200
                            miseetf = 200
                            miseetf2 = 50
                            lance(
                                fondeuro,
                                immo,
                                fondsur,
                                fondfort,
                                actions,
                                etf,
                                etf2,
                                315000,
                                20 * 12,
                                misefondeuro,
                                miseimmo,
                                misefondsur,
                                misefondfort,
                                miseactions,
                                miseetf,
                                miseetf2,
                            )
